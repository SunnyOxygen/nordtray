#!/usr/bin/python3

import subprocess


def bash(command):
    return subprocess.run(command.split(" "), stdout=subprocess.PIPE).stdout.decode('ascii')


def connect(country):
    iconPath = bash("pwd").replace("\n", "")
    bash('notify-send -a NordTray -i ' + iconPath + '/icon2_blue.svg Connecting "' + country + '..."')
    bash("nordvpn c " + country)


def isConnected():
    output = bash("nordvpn status")
    return "Disconnected" not in output


def disconnect():
    bash("nordvpn d")


def getSettings():
    output = bash("nordvpn settings")
    settings = list(filter(None, output.replace("\r", "").strip("-  ").split("\n")))
    return settings


def setSetting(name, state):
    bash("nordvpn set " + name + " " + state)


def getAllCountries():
    output = bash("nordvpn countries")
    countries = list(filter(None, output.replace("\t", "\n").replace("\r", "").strip("-  ").split("\n")))
    return countries


def getGroups():
    output = bash("nordvpn groups")
    countries = list(filter(None, output.replace("\t", "\n").replace("\r", "").strip("-  ").split("\n")))
    return countries
