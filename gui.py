#!/usr/bin/python3
import PySimpleGUIQt as sg

import handleEvents as handle
import nordvpn

# set theme of gui
sg.change_look_and_feel('BluePurple')

# get current nordvpn settings and build settingsLayout
nvpnSettings = nordvpn.getSettings()
settingsLayout = []
for s in nvpnSettings:
    row = []
    name = s.split(":")[0]
    value = s.split(":")[1][1:]
    # the label
    row.append(sg.Text(name + ":"))
    # the value
    row.append(sg.Text(value, key="text-" + name))
    # a hidden field, with the same value, for actual use
    row.append(sg.InputText(value, key=name, visible=False))
    # button to toggle the setting
    row.append(sg.Button('Change ' + name))
    settingsLayout.append(row)
    if name == "DNS":
        # in case of dns a custom dns server has to be provided
        settingsLayout.append([sg.Text('Custom DNS Server'), sg.InputText('1.1.1.1 1.0.0.1', key='DNS-Server',
                                                                          tooltip='Up to three DNS Servers, separated by whitespace')])

# build Layout
topLayout = [[sg.Text('Status: '), sg.Text(text='Disconnected', size=(15, 1), key='-OUTPUT-', text_color='red')]]
connectLayout = [[sg.Button('Quick-Connect'), sg.Button('Disconnect')]]
layout = topLayout + [[sg.Frame('Settings', settingsLayout)]] + connectLayout
window_height = 200
window_width = 470
groups = nordvpn.getGroups()[0].replace("_", " ").split(", ")
menu_def = ['', ['Open', 'Quick-Connect', 'Group Connect', groups, 'Disconnect', 'Exit']]

window = sg.Window('Nordtray', layout, size=(window_width, window_height), icon=r'icon_blue.svg')
tray = sg.SystemTray(menu=menu_def, filename=r'icon_grey.svg')
window_visible = True
window_closed = False
while True:  # Event Loop
    if window_visible:
        event, values = window.read(timeout=1000)
        if event is None:
            window_visible = False
            window_closed = True
            continue
        elif event == "Minimieren":
            window.Hide()
            window_visible = False
            continue
        if nordvpn.isConnected():
            window['-OUTPUT-'].update("Connected")
            window['-OUTPUT-'].update(text_color="green")
        else:
            window['-OUTPUT-'].update("Disconnected")
            window['-OUTPUT-'].update(text_color="red")
        handle.settingEvents(event, values, window, nordvpn)
        if event == "Quick-Connect":
            nordvpn.connect("")
        if event == "Disconnect":
            nordvpn.disconnect()

    menu_item = tray.Read(timeout=1000)
    if nordvpn.isConnected():
        tray.update(filename=r'icon_green.svg')
    else:
        tray.update(filename=r'icon_red.svg')
    if menu_item == "Open" or menu_item == sg.EVENT_SYSTEM_TRAY_ICON_DOUBLE_CLICKED:
        window_visible = True
        if window_closed:
            window_closed = False
            window = sg.Window('Nordtray', layout, size=(window_width, window_height), icon=r'icon_blue.svg')
        else:
            window.UnHide()
    elif menu_item == "Quick-Connect":
        print("Connecting")
        nordvpn.connect("")
    elif menu_item == "Disconnect":
        print("Disconnecting")
        nordvpn.disconnect()
    elif menu_item in groups:
        print("Group Connect")
        nordvpn.connect(menu_item.replace(" ", "_"))
    elif menu_item == "Exit":
        window.close()
        break
