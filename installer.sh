#!/bin/bash

installNordvpn(){
	declare -A osInfo;
	osInfo[/etc/redhat-release]=yum
	osInfo[/etc/arch-release]=pacman
	osInfo[/etc/debian_version]=apt-get

	for f in "${!osInfo[@]}"
	do
    		if [[ -f $f ]];then
        		echo Package manager: ${osInfo[$f]}
			if [[ "${osInfo[$f]}" == "yum" ]]; then
				curl -sO "https://repo.nordvpn.com/yum/nordvpn/centos/noarch/Packages/n/nordvpn-release-1.0.0-1.noarch.rpm"
				sudo yum install nordvpn-release-1.0.0-1.noarch.rpm
				sudo rpm -v --import https://repo.nordvpn.com/gpg/nordvpn_public.asc
				sudo yum update
				sudo yum install nordvpn
				nordvpn login
				returncode="SUCCESS"
				rm nordvpn-release-1.0.0-1.noarch.rpm
			elif [[ "${osInfo[$f]}" == "pacman" ]]; then
				declare  archStuff;
				archStuff[aura]=aura
				archStuff[pacaur]=pacaur
				archStuff[pakku]=pakku
				archStuff[pikaur]=pikaur
				archStuff[trizen]=trizen
				archStuff[yay]=yay
				archStuff[yaourt]=yaourt

				check=""
				for a in "${!archStuff[@]}"
				do
					if ! aur_alias="$(type -p "${archStuff[$a]}")" || [[ -z $aur_alias ]]; then
						continue
					else
						${archStuff[$a]} -S nordvpn-bin
						check="Nordvpn installed"
						break
					fi
				done
				if [[ -z "$check" ]]; then
					echo "No AUR Package Manager found."
					echo "Installing manually... careful, it will not be updated by pacman -Syu"
					curl -sO "https://aur.archlinux.org/cgit/aur.git/snapshot/nordvpn-bin.tar.gz"
					tar -xvzf nordvpn-bin.tar.gz
					rm nordvpn-bin.tar.gz
					cd nordvpn-bin
					makepkg -si
					cd ..
					rm -rf nordvpn-bin
				fi
				sudo systemctl enable nordvpnd.service
				sudo systemctl start nordvpnd.service
				nordvpn login
				returncode="SUCCESS"
			elif [[ "${osInfo[$f]}" == "apt-get" ]]; then
				curl -sO "https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn-release_1.0.0_all.deb"
				sudo apt-get install nordvpn-release_1.0.0_all.deb
				sudo apt-get update
				sudo apt-get install nordvpn
				nordvpn login
				returncode="SUCCESS"
			fi
    		fi
	done

}

setup(){
	echo "Checking Python version"
	if ! python_alias="$(type -p python3)" || [ -z "$python_alias" ]; then
		echo "Found no python3 install"
		echo "Current Python version is:"
		python --version
		exit 1
	fi
	echo "Checking whether pip is installed"
	if ! pip_alias="$(type -p pip3)" || [ -z "$pip_alias" ]; then
		echo "No pip3 install found"
		echo "Please install pip3"
		exit 1
	fi

	echo "Installing Dependencies:"
	sudo pip3 install -r "$(pwd)"/requirements.txt

	echo "Creating Desktop Entry"
	touch Nordtray.desktop
	echo "[Desktop Entry]" >> Nordtray.desktop
	echo "Name=Nordtray" >> Nordtray.desktop
	echo "Description=GUI for Nordvpn, very nice, python" >> Nordtray.desktop
	echo "Comment=Created by LBJ" >> Nordtray.desktop
	echo "Exec=python3 gui.py" >> Nordtray.desktop
	echo "Path=$(pwd)" >> Nordtray.desktop
	echo "Terminal=false" >> Nordtray.desktop
	echo "Type=Application" >> Nordtray.desktop
	echo "Icon=$(pwd)/icon_blue.svg" >> Nordtray.desktop
	echo "Categories=Utility;" >> Nordtray.desktop
	cp Nordtray.desktop /home/"$USER"/.local/share/applications/

	read -r -p "Do you want to have the application run on startup? [y/n] " autostart
	if [[ -n "$autostart" ]] && [[ "$autostart" == "y" ]]; then
		echo "Creating Autostart Entry"
		cp Nordtray.desktop /home/"$USER"/.config/autostart/
	fi
	rm Nordtray.desktop
	echo "Installation complete"
}

echo "Welcome To Nordtray, the friendly python gui for nordvpn"
echo "Checking for installed Nordvpn..."
if ! nordvpn_alias="$(type -p nordvpn)" || [[ -z $nordvpn_alias ]]; then
	echo "No command called 'nordvpn' found"
	read -r -p "Should I try to install it for you? [y/n] " installChoice
	if [[ -n "$installChoice" ]] || [[ "$installChoice" == "n" ]]; then
		echo "Okay, try again after installing NordVPN"
		echo "Official Website: https://nordvpn.com/download/linux/ "
	else
		returncode=""
		installNordvpn
		if [[ -n "$returncode" ]];then
			setup
		fi
	fi
else
	echo "Nordvpn found!"
	setup
fi

