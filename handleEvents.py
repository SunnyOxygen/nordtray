#!/usr/bin/python3


def settingEvents(event, values, window, nordvpn):
    if event == "Change Technology":
        valToSet = "wireguard" if values["Technology"] == "OpenVPN" else "openvpn"
        nordvpn.setSetting("technology", valToSet)
        if valToSet == "wireguard":
            window["text-Technology"].update("WireGuard")
            window["Technology"].update("WireGuard")
        else:
            window["text-Technology"].update("OpenVPN")
            window["Technology"].update("OpenVPN")
    if event == "Change Protocol":
        valToSet = "udp" if values["Protocol"] == "TCP" else "tcp"
        nordvpn.setSetting("protocol", valToSet)
        if valToSet == "udp":
            window["text-Protocol"].update("UDP")
            window["Protocol"].update("UDP")
        else:
            window["text-Protocol"].update("TCP")
            window["Protocol"].update("TCP")
    if event == "Change Kill Switch":
        valToSet = "enabled" if values["Kill Switch"] == "disabled" else "disabled"
        nordvpn.setSetting("killswitch", valToSet)
        if valToSet == "enabled":
            window["text-Kill Switch"].update("enabled")
            window["Kill Switch"].update("enabled")
        else:
            window["text-Kill Switch"].update("disabled")
            window["Kill Switch"].update("disabled")
    if event == "Change CyberSec":
        valToSet = "enabled" if values["CyberSec"] == "disabled" else "disabled"
        nordvpn.setSetting("cybersec", valToSet)
        if valToSet == "enabled":
            window["text-CyberSec"].update("enabled")
            window["CyberSec"].update("enabled")
        else:
            window["text-CyberSec"].update("disabled")
            window["CyberSec"].update("disabled")
    if event == "Change Obfuscate":
        valToSet = "enabled" if values["Obfuscate"] == "disabled" else "disabled"
        nordvpn.setSetting("obfuscate", valToSet)
        if valToSet == "enabled":
            window["text-Obfuscate"].update("enabled")
            window["Obfuscate"].update("enabled")
        else:
            window["text-Obfuscate"].update("disabled")
            window["Obfuscate"].update("disabled")
    if event == "Change Notify":
        valToSet = "enabled" if values["Notify"] == "disabled" else "disabled"
        nordvpn.setSetting("notify", valToSet)
        if valToSet == "enabled":
            window["text-Notify"].update("enabled")
            window["Notify"].update("enabled")
        else:
            window["text-Notify"].update("disabled")
            window["Notify"].update("disabled")
    if event == "Change Auto-connect":
        valToSet = "enabled" if values["Auto-connect"] == "disabled" else "disabled"
        nordvpn.setSetting("autoconnect", valToSet)
        if valToSet == "enabled":
            window["text-Auto-connect"].update("enabled")
            window["Auto-connect"].update("enabled")
        else:
            window["text-Auto-connect"].update("disabled")
            window["Auto-connect"].update("disabled")
    if event == "Change DNS":
        servers = window["DNS-Server"].Get()
        valToSet = servers if values["DNS"] == "disabled" else "disabled"
        nordvpn.setSetting("dns", valToSet)
        if valToSet != "disabled":
            window["text-DNS"].update(servers)
            window["DNS"].update(servers)
        else:
            window["text-DNS"].update("disabled")
            window["DNS"].update("disabled")
